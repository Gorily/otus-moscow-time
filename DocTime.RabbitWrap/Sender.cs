﻿using DocTime.Enums;
using DocTime.Tables;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;

namespace DocTime.RabbitWrap
{
    /// <summary>Отправитель. Класс реализует коннект для работы с отправкой сообщений</summary>
    public class Sender : IDisposable
    {
        private readonly ConnectionFactory _connectFactory;
        private readonly IConnection _connect;
        private readonly IModel _model;

        public Sender()
        {
            _connectFactory = new ConnectionFactory()
            {
                UserName = "hojkqvye",
                Password = "wJQTJr0J9iRdigOgKNndiXrAXfAfcIH7",
                VirtualHost = "hojkqvye",
                HostName = "bloodhound.rmq.cloudamqp.com"
            };
            _connect = _connectFactory.CreateConnection();
            _model = _connect.CreateModel();
        }

        /// <summary>Отправить документ брокеру</summary>
        public string SendMessage(string queueName, UserDocument userDocument)
        {
            _model.QueueDeclare(queue: queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            var sendMessage = JsonConvert.SerializeObject(userDocument);
            var body = Encoding.UTF8.GetBytes(sendMessage);

            _model.BasicPublish(exchange: "",
                                     routingKey: queueName,
                                     basicProperties: null,
                                     body: body);


            return $"Документ: '{userDocument.Document.Name}' отправлен брокеру.";
        }

        /// <summary>Отправить сообщение сотруднику</summary>
        public string SendMessageToUser(string queueName, UserDocument userDocument)
        {

            _model.QueueDeclare(queue: queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            string message;
            if (userDocument.Document.State == DocumentState.Accepted)
                message = $"Документ: '{userDocument.Document.Name}' всеми подписан";
            else
                message = $"\nВам поступил документ на подпись.\n" +
                    $"Данные о документе:\n" +
                    $"ID документа: {userDocument.Document.Id}\n" +
                    $"Наименование: {userDocument.Document.Name}\n" +
                    $"От сотрудника: {userDocument.User.LastName} {userDocument.User.FirstName}";

            var body = Encoding.UTF8.GetBytes(message);

            _model.BasicPublish(exchange: "",
                                     routingKey: queueName,
                                     basicProperties: null,
                                     body: body);
            return message;
        }

        public void Dispose()
        {
            _connect?.Dispose();
            _model?.Dispose();
        }
    }
}
