﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace DocTime.RabbitWrap
{
    /// <summary>Получатель. Класс реализует коннект для работы с получением сообщений</summary>
    public class Recipient : IDisposable
    {
        private readonly ConnectionFactory _connectFactory;
        private readonly IConnection _connect;
        private readonly IModel _model;
        public event EventHandler<string> ReceivedNewData;

        public Recipient()
        {
            _connectFactory = new ConnectionFactory()
            {
                UserName = "hojkqvye",
                Password = "wJQTJr0J9iRdigOgKNndiXrAXfAfcIH7",
                VirtualHost = "hojkqvye",
                HostName = "bloodhound.rmq.cloudamqp.com"
            };
            _connect = _connectFactory.CreateConnection();
            _model = _connect.CreateModel();
        }

        /// <summary>Запустить прослушивание</summary>
        public void StartListening(string queueName)
        {
            _model.QueueDeclare(queue: queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var consumer = new EventingBasicConsumer(_model);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                ReceivedNewData?.Invoke(this, message);
                _model.BasicAck(deliveryTag: ea.DeliveryTag, multiple: true);
            };

            _model.BasicConsume(queue: queueName,
                                     autoAck: false,
                                     consumer: consumer);
        }

        public void Dispose()
        {
            _connect?.Dispose();
            _model?.Dispose();
        }
    }
}
