﻿using DocTime.Enums;
using DocTime.RabbitWrap;
using DocTime.Tables;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace DocTime.MessageBroker
{
    /// <summary>Брокер очередь</summary>
    public class QueueBroker
    {
        private UserDocument _userDocument;
        private readonly Recipient _recipient;
        private readonly Sender _sender;
        private string _queueName;

        public QueueBroker(string queueName)
        {
            _queueName = queueName;
            _recipient = new Recipient();
            _sender = new Sender();
        }

        public void ListenQueue()
        {
            _recipient.StartListening(_queueName);
            _recipient.ReceivedNewData += _recipient_ReceivedNewData;
        }

        private void _recipient_ReceivedNewData(object sender, string e)
        {
            _userDocument = Deserialize<UserDocument>(e);
            if (_userDocument?.Document is null || _userDocument.User is null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{Environment.NewLine}Внимание, данные поступили с ошибкой.");
                Console.ResetColor();
                return;
            }
            HandlingNewDocumentEvent();
        }

        private T Deserialize<T>(string resource)
        {
            return JsonConvert.DeserializeObject<T>(resource);
        }

        /// <summary>Обработка события "Новый документ"</summary>
        private void HandlingNewDocumentEvent()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nПоступившее сообщение брокеру:");
            Console.ResetColor();
            Console.WriteLine($"\nПоступил новый документ на обработку.\n" +
                              $"Наименование документа: {_userDocument.Document.Name}\n" +
                              $"Владелец документа: {_userDocument.User.LastName} {_userDocument.User.FirstName}");

            CheckSignatures(_userDocument);
        }

        /// <summary>Проверить подписи.</summary>
        public void CheckSignatures(UserDocument userDocument)
        {
            var signators = userDocument.Document.Signatories.OrderBy(o => o.SequenceNumber);

            foreach (var signatory in signators)
            {
                if (!signatory.Signed)
                {
                    SendForSignature(signatory.User);
                    break;
                }

                if (signators.Last().Signed)
                {
                    userDocument.Document.State = DocumentState.Accepted; //TODO: !!! Нужно реализовать эту строку при подписании последним подписантом, после которого должно автоматически сохранять в БД (Что бы в этот сервис не подключать БД)
                    UserNoticeCompletion();
                    break;
                }
            }
        }

        /// <summary>Оповещение создателя документа о завершении процедуры подписей</summary>
        private void UserNoticeCompletion()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"\nОтправленное сообщение брокером:");
            Console.ResetColor();
            var text = _sender.SendMessageToUser($"{_userDocument.User.Id}{_userDocument.User.LastName}", _userDocument);
            Console.WriteLine(text);
        }

        /// <summary>Отправить на подпись.</summary>
        private void SendForSignature(User user)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\nОтправленное сообщение брокером:");
            Console.ResetColor();
            var text = _sender.SendMessageToUser($"{user.Id}{user.LastName}", _userDocument);
            Console.WriteLine(text);
        }
    }
}
