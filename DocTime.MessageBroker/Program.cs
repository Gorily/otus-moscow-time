﻿using System;

namespace DocTime.MessageBroker
{
    class Program
    {
        static readonly string queueName = "MessageBroker";
        static void Main(string[] args)
        {
            var queueDistributor = new QueueBroker(queueName);
            queueDistributor.ListenQueue();
            Console.ReadLine();
        }
    }
}
