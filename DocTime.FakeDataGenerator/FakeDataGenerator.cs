﻿using Bogus;
using DocTime.Domain.Enums;
using DocTime.Enums;
using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocTime.FakeDataGenerator
{
    public static class FakeDataGenerator
    {
        public static List<Document> GenerateDocuments(int dataCount, List<Department> departments)
        {
            var documents = new List<Document>();
            var docFaker = CreateDocsFaker(departments);

            for (int i = 0; i < dataCount; i++)
            {
                var document = docFaker.Generate();
                documents.Add(document);
            }

            return documents;
        }
        public static List<Department> GenerateDepartment(int dataCount)
        {
            var departments = new List<Department>();
            var departmentsFaker = CreateDepartmentFaker();

            for (int i = 0; i < dataCount; i++)
            {
                var department = departmentsFaker.Generate();
                departments.Add(department);
            }

            return departments;
        }

        public static List<Position> GeneratePositions(int dataCount)
        {
            var positions = new List<Position>();
            var positionFaker = CreatePositionFaker(positions);

            for (int i = 0; i < dataCount; i++)
            {
                var position = positionFaker.Generate();
                positions.Add(position);
            }

            return positions;
        }

        private static Faker<Position> CreatePositionFaker(List<Position> positions)
        {
            var docsFaker = new Faker<Position>()
                .RuleFor(f => f.Name, (f, u) => f.Lorem.Slug());

            return docsFaker;
        }

        public static List<DocumentTemplate> GenerateDocumentTemplates(List<Department> fakeDepartments, List<Signatory> signatories)
        {
            var rnd = new Random();
            var docs = new List<DocumentTemplate>();

            for (int i = 0; i < 100; i++)
            {
                var skip = rnd.Next(0, signatories.Count - 1);
                var take = rnd.Next(1, signatories.Count - skip);
                var docsTemplFaker = new Faker<DocumentTemplate>()
                    .RuleFor(f => f.Name, (f, u) => f.Lorem.Word())
                    .RuleFor(f => f.Department, fakeDepartments[rnd.Next(0, fakeDepartments.Count)]);

                var doc = docsTemplFaker.Generate();

                docs.Add(doc);
            }
            return docs;
        }

        public static List<DocumentTemplateSignatory> GenerateDocumentTemplatesSignatories(List<DocumentTemplate> fakeDocumentTemplates, List<Signatory> fakeSignatories)
        {
            var rnd = new Random();
            var docTemplSigns = new List<DocumentTemplateSignatory>();

            foreach (var doc in fakeDocumentTemplates)
            {
                var curSigns = fakeSignatories.ToList();
                var singnsCount = rnd.Next(1, fakeSignatories.Count > 7 ? 7 : rnd.Next(1, fakeSignatories.Count));
                for (int i = 0; i < singnsCount; i++)
                {
                    var signInx = rnd.Next(0, curSigns.Count);
                    var signTempl = new SignatoryTemplate { 
                        DocumentId = doc.Id, 
                        SequenceNumber = i, 
                        UserId = curSigns[signInx].UserId };
                    var docTemplSign = new DocumentTemplateSignatory
                    {
                        DocumentTemplate = doc,
                        SignatoryTemplate = signTempl
                    };
                    curSigns.RemoveAt(signInx);
                    docTemplSigns.Add(docTemplSign);
                }
            }

            return docTemplSigns;
        }

        public static User GenerateUser(Department department, Position position)
        {
            var usersFaker = CreateUserFaker(new List<Department> { department }, new List<Position> { position });
            var user = usersFaker.Generate();
            return user;
        }

        public static List<User> GenerateUsers(int dataCount, List<Department> departments, List<Position> positions)
        {
            var users = new List<User>();
            var usersFaker = CreateUserFaker(departments, positions);

            for (int i = 0; i < dataCount; i++)
            {
                var user = usersFaker.Generate();
                users.Add(user);
            }

            return users;
        }

        public static List<Signatory> GenerateSignatories(List<User> fakeUsers, List<Document> fakeDocuments)
        {
            var rnd = new Random();
            var signatories = new List<Signatory>();

            foreach (var doc in fakeDocuments)
            {
                var ucount = rnd.Next(1, 10);
                for (var i = 0; i < ucount; i++)
                {
                    var signed = rnd.Next(0, 2) == 1;
                    var signatory = new Signatory()
                    {
                        Document = doc,
                        SequenceNumber = i,
                        Signed = signed,
                        SignedDate = signed 
                            ? DateTime.UtcNow.AddDays(rnd.Next(-300, 1)) 
                            : default,
                        User = fakeUsers[rnd.Next(0, fakeUsers.Count - 1)]
                    };
                    signatories.Add(signatory);
                }
            }

            return signatories;
        }

        public static List<Identification> GenerateIdenties(List<User> fakeUsers)
        {
            var identifications = new List<Identification>();
            foreach (var user in fakeUsers)
            {
                identifications.Add(new Identification() { Login = user.FirstName, Password = "123", UserId = user.Id });
            }
            return identifications;
        }
        private static Faker<Document> CreateDocsFaker(List<Department> departments)
        {
            var rnd = new Random();
            var docsFaker = new Faker<Document>()
                .RuleFor(f => f.Name, (f, u) => f.Lorem.Word())
                .RuleFor(f => f.File, f => System.IO.Path.GetRandomFileName())
                .RuleFor(f => f.State, f => (DocumentState)f.Random.Int(0, 3))
                .RuleFor(f => f.Department, f => departments[rnd.Next(0, departments.Count)]);

            return docsFaker;
        }

        private static Faker<Department> CreateDepartmentFaker()
        {
            var departmentsFaker = new Faker<Department>()
            .RuleFor(u => u.Name, (f, u) => f.Commerce.Department());

            return departmentsFaker;
        }
        private static Faker<User> CreateUserFaker(List<Department> departments, List<Position> positions)
        {
            var usersFaker = new Faker<User>()
                //.CustomInstantiator(f => new User()
                //{
                //    //Id = id++
                //})
                .RuleFor(u => u.Position, (f, u) => positions[f.Random.Int(0, positions.Count - 1)])
                .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.LastName, (f, u) => f.Name.LastName())
                .RuleFor(u => u.Patronymic, (f, u) => f.Name.LastName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName))
                .RuleFor(u => u.RegistrationDateUTC, (f, u) => f.Date.Between(DateTime.Now.AddYears(-2), DateTime.Now))
                .RuleFor(u => u.BirthDate, (f, u) => f.Date.Between(DateTime.Now.AddYears(-65), DateTime.Now.AddYears(-18)))
                .RuleFor(u => u.Department, (f, u) => departments[f.Random.Int(0, departments.Count - 1)]);

            return usersFaker;
        }

        public static List<UserDocument> GenerateFakeUserDocuments(List<User> fakeUsers, List<Document> fakeDocuments)
        {
            var rnd = new Random();
            var userDocs = new List<UserDocument>();

            foreach (var user in fakeUsers)
            {
                var fdoc = fakeDocuments.Where(w => w.Department.Id == user.Department.Id).ToList();
                if (fdoc.Count == 0) { break; }
                var docCount = rnd.Next(1, fdoc.Count > 10 ? 10 : fdoc.Count);
                for (int i = 0; i < docCount; i++)
                {
                    var fakeDoc = fdoc[rnd.Next(0, fdoc.Count - 1)];
                    var userDoc = new UserDocument()
                    {
                        Document = fakeDoc,
                        User = user
                    };
                    userDocs.Add(userDoc);
                    fdoc.Remove(fakeDoc);
                }
            }

            return userDocs;
        }
    }
}
