﻿using DocTime.DAL.Context;
using DocTime.DAL.Repositories;
using DocTime.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Serilog;

namespace DocTime.ConsoleApp
{
    class Program
    {
        static void Main()
        {
            IServiceCollection serviceCollection = new ServiceCollection()
                .AddLogging();
            // var container = new Container();


            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true)
                .Build();
            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();
            logger.Information("App started");
            

            var connectionString = config.GetConnectionString("DefaultConnection");

            serviceCollection.AddDbContext<DocTimeDbContext>(options => options.UseSqlServer(connectionString));
            serviceCollection.AddScoped<IIdentityRepositoryLocal, IdentityRepositoryLocal>();
            serviceCollection.AddScoped<IDbRepository, MsSqlMdfRepository>();
            serviceCollection.AddScoped<AdminMode>();
            serviceCollection.AddLogging(builder =>
            {
                builder.AddSerilog(logger, true);
            });

            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
                


            Console.WriteLine("Команды" +
                              "\n1 - Создать фейковые данные;" +
                              "\n2 - Получить информацию по всем пользователям;" +
                              "\n3 - Получить список существующих документов;" +
                              "\n4 - Получить подписи по номеру документа;" +
                              "\n5 - Добавит шаблон документа (захардкожено);" +
                              "\n6 - Показать все существующие шаблоны документов." +
                              "\n7 - Показать информацию по id пользователя.");

            var admin = serviceProvider.GetService<AdminMode>();

            var c = Console.ReadLine();
            switch (c)
            {
                case "1":
                    admin.InitializeFakeData();
                    break;
                case "2":
                    admin.ShowAllUsers();
                    break;
                case "3":
                    admin.ShowAllDocuments();
                    break;
                case "4":
                    admin.ShowSignatoriesByDocId();
                    break;
                case "5":
                    admin.AddDocumentTemplate();
                    break;
                case "6":
                    admin.ShowDocumentTemplates();
                    break;
                case "7":
                    admin.ShowUserById();
                    break;
            }
            Log.CloseAndFlush();
        }
    }
}