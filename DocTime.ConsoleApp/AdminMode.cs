﻿using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace DocTime.ConsoleApp
{
    public class AdminMode
    {
        private readonly IIdentityRepositoryLocal _identity;
        private readonly IDbRepository _data;
        private readonly ILogger _logger;

        public AdminMode(IIdentityRepositoryLocal identity, IDbRepository data, ILogger<AdminMode> logger)
        {
            _identity = identity;
            _data = data;
            _logger = logger;
        }
        public void InitializeFakeData()
        {
            List<Position> fakePositions = FakeDataGenerator.FakeDataGenerator.GeneratePositions(20);
            _data.AddPositions(fakePositions);
            var fakeDepartments = FakeDataGenerator.FakeDataGenerator.GenerateDepartment(20);
            _data.AddDepartments(fakeDepartments);
            var fakeUsers = FakeDataGenerator.FakeDataGenerator.GenerateUsers(100, fakeDepartments, fakePositions);
            _identity.AddUsers(fakeUsers);
            var fakeIdenties = FakeDataGenerator.FakeDataGenerator.GenerateIdenties(fakeUsers);
            _identity.AddIdentifications(fakeIdenties);
            var fakeDocuments = FakeDataGenerator.FakeDataGenerator.GenerateDocuments(100, fakeDepartments);
            _data.AddDocuments(fakeDocuments);
            var userDocuments = FakeDataGenerator.FakeDataGenerator.GenerateFakeUserDocuments(fakeUsers, fakeDocuments);
            _data.AddUserDocuments(userDocuments);
            var fakeSignatories = FakeDataGenerator.FakeDataGenerator.GenerateSignatories(fakeUsers, fakeDocuments);
            _data.AddSignatories(fakeSignatories);
            var fakeDocumentTemplates = FakeDataGenerator.FakeDataGenerator.GenerateDocumentTemplates(fakeDepartments, fakeSignatories);
            _data.AddDocumentTemplates(fakeDocumentTemplates);
            var fakeDocumentTemplatesSignatories = FakeDataGenerator.FakeDataGenerator.GenerateDocumentTemplatesSignatories(fakeDocumentTemplates, fakeSignatories);
            _data.AddDocumentTemplatesSignatories(fakeDocumentTemplatesSignatories);
        }

        public void ShowAllUsers()
        {
            _logger.LogDebug("Begin fetching user");
            var users = _data.GetUsers();
            _logger.LogDebug("All users fetched from DB");
            foreach (var user in users)
            {
                Console.WriteLine($"Id: {user.Id}, {user.FirstName}, {user.LastName}, {user.Patronymic}, Position: {user.Position.Name}, {user.RegistrationDateUTC}, {user.State}, {nameof(user.BirthDate)} {user.BirthDate}, Department: {user.Department.Name}, Documents count {user.Documents.Count()}");
            }
            _logger.LogInformation("ShowAllUser done");
            Console.ReadLine();
        }

        internal void ShowDocumentTemplates()
        {
            var docTemplates = _data.GetDocTemplates();
            var data = string.Join("\n\n", docTemplates.Select(s => $"Шаблон #{s.Id}\nИмя: {s.Name};\nЧисло подписей: {s.Signatories.Count()}."));
            Console.WriteLine(data);
            Console.ReadLine();
        }

        internal void ShowUserById()
        {
            var user = _data.GetUserById(54);
            Console.WriteLine(user.Id);
            Console.ReadLine();
        }

        //public void ShowAllUsers()
        //{
        //    var users = _data.GetUsers();
        //    foreach (var user in users)
        //    {
        //        Console.WriteLine($"Id: {user.Id}, {user.FirstName}, {user.LastName}, {user.Patronymic}, Position: {user.Position.Name}, {user.RegistrationDateUTC}, {user.State}, {nameof(user.BirthDate)} {user.BirthDate}, Department: {user.Department.Name}, Documents count {user.Documents.Count()}");
        //    }

        //    Console.ReadLine();
        //}
        public void ShowAllDocuments()
        {
            var documents = _data.GetDocuments();
            var data = string.Join("\n\n", documents.Select(s => $"Документ #{s.Id}\nИмя: {s.Name}.\nФайл: {s.File}.\nСостояние: {s.State}.\nЧисло подписей: {s.Signatories.Count()}."));

            Console.WriteLine(data);
            Console.ReadLine();
        }
        public void ShowSignatoriesByDocId()
        {
            Console.WriteLine("Введите Id документа: ");
            var id = Console.ReadLine();
            var result = int.TryParse(id, out int docId);
            var signatories = result
                ? _data.GetSignatoriesByDocId(docId) 
                : new List<Signatory>();
            if (signatories is null) { return; }
            if (signatories.Any() == false) { return; }

            var data = string.Join("\n\n", signatories.Select(s => $"Подпись #{s.Id}\nПодписано: {s.Signed}.\nДата подписи: {s.SignedDate}.\nНомер очередности: {s.SequenceNumber}.\nСотрудник подписант id: {s.UserId}."));

            Console.WriteLine(data);
            Console.ReadLine();
        }
        public void AddDocumentTemplate()
        {
            //var signs = _data.GetSignatoriesByDocId(12);
            //var dep = _data.GetDepartmentById(7);
            //var docTemplate = new DocumentTemplate() { Department = dep, Name = "Новый", Signatories = signs };
            //_data.AddDocumentTemplate(docTemplate);
        }
    }
}
