﻿using DocTime.Application.ConsoleUI;
using DocTime.Application.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using Serilog;

namespace DocTime.WebClient
{
    class Program
    {
        static void Main(string[] args)
        {

            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory
                .GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true);
            var config = configBuilder.Build();
            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();
            logger.Information("App started");

            var webApiBaseAddress = config["WebApiBaseAddress"];

            var uInterface = new ConsoleUI();
            var dProvider = new ApiDataProvider(webApiBaseAddress);
            var dc = new ApiDataClient(uInterface, dProvider);
            dc.LoginCommands();
            Log.CloseAndFlush();
            Console.ReadLine();
        }
    }
}
