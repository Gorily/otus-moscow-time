﻿using DocTime.Application.ConsoleUI.Abstractions;
using DocTime.Application.ConsoleUI.Enums;
using DocTime.Application.Services.Abstractions;
using DocTime.DTO;
using DocTime.RabbitWrap;
using DocTime.Tables;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DocTime.WebClient
{
    class ApiDataClient
    {
        private readonly IConsoleUI _ui;
        private readonly IApiDataProvider _dataProvider;
        private readonly Sender _sender;
        private readonly Recipient _recipient;
        private User currentUser;
        private const string adminLogin = "admin";
        private const string adminPassword = "admin";

        /// <summary> Документы требующие подписи по очереди. </summary>
        private Document[] firstSignDocuments = Array.Empty<Document>();

        public ApiDataClient(IConsoleUI userInterface, IApiDataProvider dataProvider)
        {
            _ui = userInterface;
            _dataProvider = dataProvider;

            _sender = new Sender();
            _recipient = new Recipient();
        }

        private async Task ConnectAsync()
        {
            try
            {
                _ui.WriteLine("Подключение к DocTime API...");
                var result = await _dataProvider.InitApiAsync();
                _ui.WriteLine($"Ответ: {result}");
                _ui.WriteLine($"Соединение успешно установлено.");
            }
            catch (Exception ex)
            {
                _ui.WriteLine(ex.ToString());
            }
        }

        public void LoginCommands()
        {
            _ui.WriteLine("\nКоманды DocTime API:");
            _ui.WriteLine("1 - Войти в систему;");
            _ui.WriteLine("0 - выход.");

            var result = _ui.GetInput("Введите команду:");
            LoginCommandExecutor(result).GetAwaiter().GetResult();
        }

        public void GetAdminCommands()
        {
            _ui.WriteLine("\nПанель администратора DocTime:");
            _ui.WriteLine("1 - получить пользователя по id;");
            _ui.WriteLine("2 - получить всех пользователей (возможно длительное выполнение);");
            //_ui.WriteLine("3 - генерация случайного пользователя;");
            _ui.WriteLine("4 - переименовать пользователя по id;");
            _ui.WriteLine("5 - получить максимальный id пользователя;");
            _ui.WriteLine("6 - удалить пользователя по id;");
            _ui.WriteLine("0 - выход.");

            var result = _ui.GetInput("Введите команду:");
            AdminCommandsExecutor(result).GetAwaiter().GetResult();
        }

        private async Task LoginCommandExecutor(string command)
        {
            var access = false;
            _ui.Clear();
            switch (command)
            {
                case "1":
                    access = !(await LoginUserAsync() is null);
                    break;
                case "0":
                    _ui.Write("Досвидания!");
                    return;
                default:
                    _ui.Write("Недопустимая команда!\n");
                    break;
            }

            if (access)
            {
                _ui.WriteLine("Вы успешно авторизовались.");

                _recipient.StartListening($"{currentUser.Id}{currentUser.LastName}");
                _recipient.ReceivedNewData += _recipient_ReceivedNewData;

                if (currentUser == null)
                {
                    GetAdminCommands();
                }
                else
                {
                    GetUserCommands();
                }
            }
            else
            {
                _ui.WriteLine("Недопустимый логин или пароль!");
                LoginCommands();
            }
        }

        private void _recipient_ReceivedNewData(object sender, string e)
        {
            _ui.WriteMessageLine(e, MessageLevel.Notification);
        }

        private void GetUserCommands()
        {
            _ui.WriteLine($"\nКоманды DocTime API для пользователя id - {currentUser.Id}:");
            _ui.WriteLine("1 - получить список инициированных документов;");
            _ui.WriteLine("2 - инициировать документ;");
            _ui.WriteLine("3 - документы требующие подписи;");
            _ui.WriteLine("0 - выход.");

            var command = _ui.GetInput("Введите команду:");
            UserCommandsExecutorAsync(command).GetAwaiter().GetResult();
        }

        private async Task UserCommandsExecutorAsync(string command)
        {
            _ui.Clear();
            switch (command)
            {
                case "1":
                    GetUserDocuments();
                    break;
                case "2":
                    await InitialDocAsync();
                    break;
                case "3":
                    await GetDocsRequiringSignature();
                    break;
                case "0":
                    _ui.Write("Досвидания!");
                    return;
                default:
                    _ui.Write("Недопустимая команда!\n");
                    break;
            }
            GetUserCommands();
        }

        private async Task GetDocsRequiringSignature()
        {
            var docs = await _dataProvider.GetDocumentsRequiringSignature(currentUser);
            if (docs is null || docs.Any() == false)
            {
                _ui.Clear();
                _ui.WriteLine("Документы требующие Вашей подписи не найдены.");
                return;
            }

            firstSignDocuments = docs
                .Where(w => w.Signatories
                    .First(f => f.UserId == currentUser.Id).SequenceNumber == w.Signatories
                    .Where(w => w.Signed == false)
                    .Min(m => m.SequenceNumber))
                .ToArray();

            if (firstSignDocuments != null)
            {
                docs = docs.Except(firstSignDocuments).ToList();
            }

            if (docs.Count != 0)
            {
                _ui.WriteLine("Документы в очереди на подпись:");
                for (var i = 0; i < docs.Count; i++)
                {
                    _ui.WriteLine($"{docs[i].ToStringList(i + 1)}");
                }
            }

            if (firstSignDocuments.Any() == false) { return; }
            _ui.WriteLine($"{Environment.NewLine}Документы ожидающие Вашей подписи:");
            _ui.WriteLine(string.Join($"{Environment.NewLine}",
                firstSignDocuments.Select((s, i) => s.ToStringList(i + 1)))
                , MessageLevel.Attention);

            await GetUserSignCommandsAsync();
        }

        private async Task GetUserSignCommandsAsync()
        {
            _ui.WriteLine($"\nКоманды для подписания документов:");
            _ui.WriteLine("N - подписать документ;");
            _ui.WriteLine("0 - выход.");

            var parse = int.TryParse(_ui.GetInput("Укажите номер документа: "), out var inx);
            if (inx < 1) { return; }
            if (inx > firstSignDocuments.Count())
            {
                _ui.Clear();
                _ui.WriteLine("Указан неверный номер документа!");
                return;
            }

            await SignDocumentByIdAsync(inx);
        }

        private async Task SignDocumentByIdAsync(int inx)
        {
            var signDocDto = new SignDocDto(currentUser.Id, firstSignDocuments[inx - 1].Id);
            var res = await _dataProvider.SignDocumentByIdAndUserIdAsync(signDocDto);
            if (res is null)
            {
                _ui.WriteLine($"Не удалось подписать документ n - {inx}, id - {firstSignDocuments[inx - 1].Id}.");
            }
            else
            {
                _ui.WriteLine(_sender.SendMessage("MessageBroker", res));
                _ui.WriteLine($"Документ '{res?.Document?.Name}' Вами подписан.");
            }
        }

        private async Task InitialDocAsync()
        {
            var docs = await _dataProvider.GetDepartmentDocumentTemplatesAsync(currentUser.Department.Id);
            if (docs is null) { return; }

            _ui.WriteLine("Доступные документы:");
            for (var i = 0; i < docs.Count(); i++)
            {
                _ui.WriteLine($"{i + 1}) id: {docs[i].Id}, имя: {docs[i].Name }.");
            }
            var parse = int.TryParse(_ui.GetInput("Укажите номер документа: "), out var templInx);
            if (parse is false || templInx > docs.Count() || templInx < 1)
            {
                _ui.Clear();
                _ui.WriteLine("Указан неверный номер документа!");
                return;
            }
            templInx--;
            var fname = _ui.GetInput("Укажите имя файла документа: ");
            var initDoc = new InitialDocDto(currentUser, docs[templInx], fname);

            var userDoc = await _dataProvider.InitialDocAsync(initDoc);
            var user = await _dataProvider.GetUserByIdAsync(currentUser.Id.ToString());

            if (user != null && user.Id == currentUser.Id)
            {
                currentUser = user;

                _ui.WriteLine(_sender.SendMessage("MessageBroker", userDoc));
                _ui.WriteLine($"Документ {initDoc.DocumentTemplate.Name} успешно инициирован.");
            }
            else
            {
                _ui.WriteLine($"Ошибка инициализации документа.");
            }
        }

        private void GetUserDocuments()
        {
            _ui.WriteLine($"Всего документов: {currentUser.Documents.Count()}");
            if (currentUser.Documents is null) { return; }

            _ui.WriteLine($"{Environment.NewLine}Список документов:");
            var docs = currentUser.Documents.OrderBy(o => o.DocumentId).ToArray();
            for (int i = 0; i < docs.Length; i++)
            {
                _ui.WriteLine($"{i + 1}) id: {docs[i].DocumentId}, имя: {docs[i].Document.Name }, статус: {docs[i].Document.State}.");
            }
        }

        private async Task<int?> LoginUserAsync()
        {
            _ui.Clear();

            var login = _ui.GetInput("Введите логин: ");
            var password = _ui.GetInput("Введите пароль: ");

            if (login == adminLogin && password == adminPassword)
            { return -1001; }

            currentUser = await GetAccessAsync(login, password);
            return currentUser?.Id;
        }

        private async Task<User> GetAccessAsync(string login, string password)
        {
            var logInf = new DTO.LoginInfoDto { Login = login, Password = password };
            var res = await _dataProvider.GetAccessAsync(logInf);
            return res;
        }

        private async Task AdminCommandsExecutor(string command)
        {
            _ui.Clear();
            switch (command)
            {
                case "1":
                    await GetUserDataByIdAsync();
                    break;
                case "2":
                    await GetAllUsersDataAsync();
                    break;
                case "3":
                    await AddFakeUserAsync();
                    break;
                case "4":
                    await RenameUserByIdAsync();
                    break;
                case "5":
                    await GetUserMaxIdAsync();
                    break;
                case "6":
                    await DeleteUserByIdAsync();
                    break;
                case "0":
                    _ui.Write("Досвидания!");
                    return;
                default:
                    _ui.Write("Недопустимая команда!\n");
                    break;
            }
            GetAdminCommands();
        }

        private Task RenameUserByIdAsync()
        {
            throw new NotImplementedException();
        }

        private async Task DeleteUserByIdAsync()
        {
            //var id = _ui.GetInput("Input customer id:");

            //User user;
            //try
            //{
            //    customer = await _dataProvider.GetUserByIdAsync(id);
            //}
            //catch (HttpRequestException ex)
            //{
            //    _ui.WriteLine(ex.Message);
            //    return null;
            //}

            //_ui.WriteLine("Customer found:");

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(customer);
            //_ui.WriteLine(json);

            //return customer;
        }

        private Task GetUserMaxIdAsync()
        {
            throw new NotImplementedException();
        }

        //private Task RenameUserByIdAsync()
        //{
        //var user = await GetUserByIdAsync();
        //if (customer is null) { return; }
        //var name = _ui.GetInput("Enter the customer full name: ");
        //if (string.IsNullOrEmpty(name))
        //{
        //    _ui.WriteLine("Invalid customer name!");
        //    return;
        //}
        //customer.FullName = name;
        //var renamedCustomer = await _dataProvider.RenameCustomerAsync(customer);
        //if (renamedCustomer is null) { return; }
        //_ui.WriteLine("Customer information has been updated.");
        //}

        private Task AddFakeUserAsync()
        {
            throw new NotImplementedException();
        }

        private async Task GetAllUsersDataAsync()
        {
            _ui.WriteLine("Пожалуйста подождите...");
            var res = await _dataProvider.GetAllUsersDataAsync();
            _ui.WriteLine(res);
        }

        private async Task GetUserDataByIdAsync()
        {
            var res = string.Empty;
            var id = _ui.GetInput("Введите id пользователя:");

            //try
            //{
            res = await _dataProvider.GetUserDataByIdAsync($@"users\{id}");
            //}
            //catch (HttpRequestException ex)
            //{
            //    _ui.WriteLine(ex.Message);
            //}

            _ui.WriteLine(res);
        }
    }
}
