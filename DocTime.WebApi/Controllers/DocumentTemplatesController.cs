﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DocTime.DAL.Context;
using DocTime.Tables;
using DocTime.Repositories.Abstractions;

namespace DocTime.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentTemplatesController : ControllerBase
    {
        private readonly IDocumentTemplateRepository _templRep;

        public DocumentTemplatesController(IDocumentTemplateRepository templRep)
        {
            _templRep = templRep;
        }

        // GET: api/DocumentTemplates
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DocumentTemplate>>> GetDocumentTemplates()
        {
            return Ok(await _templRep.GetAllAsync());
        }

        // GET: api/DocumentTemplates/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DocumentTemplate>> GetDocumentTemplate(int id)
        {
            var documentTemplate = await _templRep.GetByIdAsync(id);

            if (documentTemplate == null)
            {
                return NotFound();
            }

            return documentTemplate;
        }

        // GET: api/DocumentTemplates/Department/5
        [HttpGet("Department/{id}")]
        public async Task<ActionResult<List<DocumentTemplate>>> GetTemplatesByDepartmentId(int id)
        {
            var documents = await _templRep.GetTemplatesByDepartmentId(id);

            if (documents is null)
            {
                return NotFound();
            }

            return Ok(documents);
        }

        // PUT: api/DocumentTemplates/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDocumentTemplate(int id, DocumentTemplate documentTemplate)
        {
            await _templRep.UpdateAsync(documentTemplate);
            return Ok();
        }

        // POST: api/DocumentTemplates
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DocumentTemplate>> PostDocumentTemplate(DocumentTemplate documentTemplate)
        {
            await _templRep.AddAsync(documentTemplate);
            return CreatedAtAction("GetDocumentTemplate", new { id = documentTemplate.Id }, documentTemplate);
        }

        // DELETE: api/DocumentTemplates/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DocumentTemplate>> DeleteDocumentTemplate(int id)
        {
            var documentTemplate = await _templRep.GetByIdAsync(id);
            if (documentTemplate == null)
            {
                return NotFound();
            }

            await _templRep.DeleteAsync(documentTemplate);

            return documentTemplate;
        }
    }
}
