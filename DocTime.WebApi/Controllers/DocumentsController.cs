﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DocTime.DAL.Context;
using DocTime.Tables;
using DocTime.Repositories.Abstractions;
using DocTime.DTO;
using System.IO;
using DocTime.Application.Factories;

namespace DocTime.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        private readonly IDocumentRepository _docRep;
        private readonly IUserDocumentRepository _userDocRep;
        private readonly IRepository<Signatory> _signRep;

        public DocumentsController(IDocumentRepository docRep, IUserDocumentRepository userDocRep, IRepository<Signatory> signRep)
        {
            _docRep = docRep;
            _userDocRep = userDocRep;
            _signRep = signRep;
        }

        // GET: api/Documents
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Document>>> GetDocuments()
        {
            return Ok(await _docRep.GetAllAsync());
        }

        // GET: api/Documents/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Document>> GetDocument(int id)
        {
            var document = await _docRep.GetByIdAsync(id);

            if (document == null)
            {
                return NotFound();
            }

            return document;
        }

        // GET: api/Documents/Department/5
        [HttpGet("Department/{id}")]
        public async Task<ActionResult<List<Document>>> GetDocumentsByDepartmentId(int id)
        {
            var documents = await _docRep.GetDocsByDepartmentId(id);

            if (documents is null)
            {
                return NotFound();
            }

            return Ok(documents);
        }

        [HttpPost("docsrequiringsign")]
        public async Task<ActionResult<Document>> GetDocsRequiringSignatureByUser(UserDto user)
        {
            var docs = await _docRep.GetDocsRequiringSignatureByUser(user);

            return Ok(docs);
        }

        [HttpPost("init")]
        public async Task<ActionResult<UserDocument>> InitDocument(InitialDocDto initialDoc)
        {
            var doc = new Document
            {
                DepartmentId = initialDoc.User.Department.Id,
                File = initialDoc.DocumentFile,
                Name = initialDoc.DocumentTemplate.Name,
                State = Enums.DocumentState.Active
            };

            await _docRep.AddAsync(doc);

            var signatories = SignatoryFactory.CreateSignatories(initialDoc.DocumentTemplate.Signatories, doc);
            await _signRep.AddRangeAsync(signatories);

            doc.Signatories = signatories;
            await _docRep.UpdateAsync(doc);

            doc = await _docRep.GetByIdWithSignatoryUsersAsync(doc.Id);

            var userDoc = new UserDocument {DocumentId = doc.Id, UserId = initialDoc.User.Id};
            await _userDocRep.AddAsync(userDoc);
            userDoc = await _userDocRep.GetByIdAsync(userDoc.Id);

            return Ok(userDoc);
        }

        [HttpPost("signdoc")]
        public async Task<ActionResult<UserDocument>> SignDocument(SignDocDto signDocDto)
        {
            var doc = await _docRep.GetByIdWithSignatoryUsersAsync(signDocDto.DocumentId);

            var sign = doc.Signatories.FirstOrDefault(s => s.UserId == signDocDto.UserId);
            sign.SignedDate = DateTime.UtcNow;
            sign.Signed = true;

            var allSign = doc.Signatories.All(a => a.Signed == false);
            if (allSign) 
            { doc.State = Enums.DocumentState.Accepted; }

            await _docRep.UpdateAsync(doc);

            var userDoc = await _userDocRep.GetUserDocumentByDocumentIdAsync(doc.Id);

            return Ok(userDoc);
        }

        // PUT: api/Documents/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDocument(int id, Document document)
        {
            await _docRep.UpdateAsync(document);
            return Ok();
        }

        // POST: api/Documents
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Document>> PostDocument(Document document)
        {
            await _docRep.AddAsync(document);
            return CreatedAtAction("GetDocument", new { id = document.Id }, document);
        }

        // DELETE: api/Documents/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Document>> DeleteDocument(int id)
        {
            var document = await _docRep.GetByIdAsync(id);
            if (document == null)
            {
                return NotFound();
            }

            await _docRep.DeleteAsync(document);

            return document;
        }
    }
}
