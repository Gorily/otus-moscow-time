﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DocTime.DAL.Context;
using DocTime.Tables;
using DocTime.Repositories.Abstractions;
using DocTime.DTO;

namespace DocTime.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentificationsController : ControllerBase
    {
        private readonly IIdentityRepository _identityRep;

        public IdentificationsController(IIdentityRepository identity)
        {
            _identityRep = identity;
        }

        // GET: api/Identifications
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Identification>>> GetIdentifications()
        {
            return Ok(await _identityRep.GetAllAsync());
        }

        // GET: api/identifications/login/?name=a&password=b
        [HttpPost("login")]
        public async Task<ActionResult<int?>> GetAccess(LoginInfoDto loginInfo)
        {
            var userId = await _identityRep.AccessAsync(loginInfo.Login, loginInfo.Password);

            if (userId is null)
            {
                return NotFound();
            }
            else 
            {
                return Ok(userId);
            }
        }
    }
}
