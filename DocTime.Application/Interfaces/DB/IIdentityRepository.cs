﻿using DocTime.Tables;
using System.Collections.Generic;

namespace DocTime.Application.Interfaces.DB
{
    public interface IIdentityRepository
    {
        void AddUser(User user);
        void AddUsers(List<User> users);
        bool Access(string login, string password);
        void AddIdentification(Identification identification);
        void AddIdentifications(List<Identification> identifications);
    }
}
