﻿using DocTime.Tables;
using System.Collections.Generic;

namespace DocTime.Application.Interfaces.DB
{
    public interface IDbRepository
    {
        void SetNewPosition(User user, Position position);
        void AddDepartment(Department department);
        void AddDepartments(List<Department> departments);
        void AddDocument(Document document);
        void AddDocuments(List<Document> documents);
        void AddSignatory(Signatory signatory);
        void AddSignatories(List<Signatory> signatories);
        void AddUserDocument(UserDocument userDocument);
        void AddUserDocuments(List<UserDocument> userDocuments);
        void AddPosition(Position position);
        void AddPositions(List<Position> positions);
        List<User> GetUsers();
    }
}
