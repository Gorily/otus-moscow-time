﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Application.ConsoleUI.Enums
{
    public enum MessageLevel
    {
        None = 0,
        Info = 1,
        Warning = 2,
        Error = 3,
        Attention = 15,
        Notification = 6
    }
}
