﻿using System;
using System.Collections.Generic;
using System.Text;
using DocTime.Application.ConsoleUI.Enums;

namespace DocTime.Application.ConsoleUI.Abstractions
{
    public interface IConsoleUI
    {
        void WriteLine(string message, MessageLevel level);
        void WriteLine(string message);
        void Write(string message);
        string GetInput(string id);
        void Clear();
        void WriteMessageLine(string message, MessageLevel level);
    }
}
