﻿using DocTime.Application.ConsoleUI.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using DocTime.Application.ConsoleUI.Enums;

namespace DocTime.Application.ConsoleUI
{
    public class ConsoleUI: IConsoleUI
    {
        public void Write(string message)
        {
            Console.Write(message);
        }

        public void WriteLine(string message, MessageLevel level)
        {
            Console.ForegroundColor = (ConsoleColor)level;
            WriteLine(message);
            Console.ResetColor();
        }

        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public string GetInput(string message)
        {
            Write(message);
            return Console.ReadLine();
        }

        public void Clear()
        {
            Console.Clear();
        }

        public void WriteMessageLine(string message, MessageLevel level)
        {
            var top = Console.CursorTop;
            var left = Console.CursorLeft;

            Console.CursorTop = top + 1;
            Console.CursorLeft = 0;

            WriteLine(message, level);

            Console.CursorTop = top;
            Console.CursorLeft = left;
        }
    }
}
