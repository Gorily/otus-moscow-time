﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Application.Factories
{
    public static class SignatoryFactory
    {
        public static Signatory CreateSignatory(DocumentTemplateSignatory signatoryTemplate, Document document)
        {
            var signatory = new Signatory
            {
                DocumentId = document.Id,
                SequenceNumber = signatoryTemplate.SignatoryTemplate.SequenceNumber,
                Signed = false,
                UserId = signatoryTemplate.SignatoryTemplate.UserId
            };
            return signatory;
        }

        public static List<Signatory> CreateSignatories(IEnumerable<DocumentTemplateSignatory> signatoryTemplate, Document document)
        {
            var signatories = new List<Signatory>();
            foreach (var signTempl in signatoryTemplate)
            {
                var signatory = new Signatory
                {
                    DocumentId = document.Id,
                    SequenceNumber = signTempl.SignatoryTemplate.SequenceNumber,
                    Signed = false,
                    UserId = signTempl.SignatoryTemplate.UserId
                };
                signatories.Add(signatory);
            }

            return signatories;
        }
    }
}
