﻿using DocTime.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using DocTime.Tables;

namespace DocTime.Application.Factories
{
    public static class UserDtoFactory
    {
        public static UserDto CreateUserDto(User user)
        {
            var userDto = new UserDto
            {
                BirthDate = user.BirthDate,
                Department = user.Department,
                Documents = user.Documents,
                Email = user.Email,
                FirstName = user.FirstName,
                Id = user.Id,
                LastName = user.LastName,
                Patronymic = user.Patronymic,
                Position = user.Position,
                RegistrationDateUTC = user.RegistrationDateUTC,
                State = user.State
            };
            return userDto;
        }
    }
}
