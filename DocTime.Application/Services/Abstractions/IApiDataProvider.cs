﻿using DocTime.DTO;
using DocTime.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DocTime.Application.Services.Abstractions
{
    public interface IApiDataProvider
    {
        Task<User> GetAccessAsync(LoginInfoDto loginInfo);
        Task<string> GetAsync(string url);
        Task<string> InitApiAsync();
        Task<string> GetUserDataByIdAsync(string id);
        Task<User> GetUserByIdAsync(string id);
        Task<string> GetAllUsersDataAsync();
        Task<int> GetUserMaxIdAsync();
        Task<User> AddNewUserAsync(User user);
        Task<User> RenameUserAsync(User user);
        Task<string> DeleteUserByIdAsync(string id);
        Task<List<Document>> GetDepartmentDocumentsAsync(int id);
        Task<List<DocumentTemplate>> GetDepartmentDocumentTemplatesAsync(int id);
        Task<UserDocument> InitialDocAsync(InitialDocDto initDoc);
        Task<List<Document>> GetDocumentsRequiringSignature(User user);
        Task<UserDocument> SignDocumentByIdAndUserIdAsync(SignDocDto signDocDto);
    }
}
