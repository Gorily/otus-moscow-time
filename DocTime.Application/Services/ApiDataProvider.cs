﻿using DocTime.Application.Factories;
using DocTime.Application.Services.Abstractions;
using DocTime.DTO;
using DocTime.Tables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DocTime.Application.Services
{
    public class ApiDataProvider : IApiDataProvider
    {
        private readonly HttpClient _httpClient;

        public ApiDataProvider(string baseAddress)
        {
            _httpClient = new HttpClient() { BaseAddress = new Uri(baseAddress) };
        }

        public async Task<User> AddNewUserAsync(User customer)
        {
            throw new NotImplementedException();
        }

        public async Task<string> DeleteUserByIdAsync(string customerId)
        {
            throw new NotImplementedException();
        }

        public async Task<User> GetAccessAsync(LoginInfoDto loginInfo)
        {
            var content = new StringContent(JsonConvert.SerializeObject(loginInfo), Encoding.UTF8, "application/json");
            var responseMessage = await _httpClient.PostAsync($@"identifications\login", content);
            if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var id = await responseMessage.Content.ReadAsStringAsync();
                var user = await GetUserByIdAsync(id);
                return user;
            }
            else
            {
                return null;
            }
        }

        public async Task<string> GetAllUsersDataAsync()
        {
            var res = await GetAsync("Users");
            return res;
        }

        public async Task<string> GetAsync(string url)
        {
            var resp = await _httpClient.GetAsync(url);
            resp.EnsureSuccessStatusCode();
            var result = await resp.Content.ReadAsStringAsync();
            return result;
        }

        public async Task<User> GetUserByIdAsync(string id)
        {
            var res = await GetMessageAsync($"Users/{id}");

            res.EnsureSuccessStatusCode();

            var content = await res.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<User>(content);
            return result;
        }

        public async Task<HttpResponseMessage> GetMessageAsync(string url)
        {
            var resp = await _httpClient.GetAsync(url);
            return resp;
        }

        public async Task<string> GetUserDataByIdAsync(string id)
        {
            var result = await GetAsync(id);
            return result;
        }

        public async Task<int> GetUserMaxIdAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<string> InitApiAsync()
        {
            var result = await GetAsync(string.Empty);
            return result;
        }

        public async Task<User> RenameUserAsync(User customer)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Document>> GetDepartmentDocumentsAsync(int departmentId)
        {
            var res = await GetMessageAsync($"Documents/Department/{departmentId}");

            res.EnsureSuccessStatusCode();

            var content = await res.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<List<Document>>(content);

            return result;
        }

        public async Task<List<DocumentTemplate>> GetDepartmentDocumentTemplatesAsync(int departmentId)
        {
            var res = await GetMessageAsync($"DocumentTemplates/Department/{departmentId}");

            res.EnsureSuccessStatusCode();

            var content = await res.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<List<DocumentTemplate>>(content);

            return result;
        }

        public async Task<UserDocument> InitialDocAsync(InitialDocDto initDoc)
        {
            var content = new StringContent(JsonConvert.SerializeObject(initDoc), Encoding.UTF8, "application/json");
            var responseMessage = await _httpClient.PostAsync($@"documents\init", content);
            if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await responseMessage.Content.ReadAsStringAsync();
                var doc = JsonConvert.DeserializeObject<UserDocument>(result);
                return doc;
            }
            else
            {
                return null;
            }
        }

        public async Task<List<Document>> GetDocumentsRequiringSignature(User user)
        {
            var userDto = UserDtoFactory.CreateUserDto(user);
            var content = new StringContent(JsonConvert.SerializeObject(userDto), Encoding.UTF8, "application/json");
            var responseMessage = await _httpClient.PostAsync($@"documents\docsrequiringsign", content);

            responseMessage.EnsureSuccessStatusCode();

            var result = await responseMessage.Content.ReadAsStringAsync();
            var docs = JsonConvert.DeserializeObject<List<Document>>(result);

            return docs;
        }

        public async Task<UserDocument> SignDocumentByIdAndUserIdAsync(SignDocDto signDocDto)
        {
            var content = new StringContent(JsonConvert.SerializeObject(signDocDto), Encoding.UTF8, "application/json");
            var responseMessage = await _httpClient.PostAsync($@"documents\signdoc", content);

            var doc = new UserDocument();
            if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await responseMessage.Content.ReadAsStringAsync();
                doc = JsonConvert.DeserializeObject<UserDocument>(result);
            }

            return doc;
        }
    }
}
