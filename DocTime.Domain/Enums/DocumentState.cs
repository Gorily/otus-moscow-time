﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Enums
{
    public enum DocumentState
    {
        None = 0,
        Active = 1,
        Rejected = 2,
        Accepted = 3
    }
}
