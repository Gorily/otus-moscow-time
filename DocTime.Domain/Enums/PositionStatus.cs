﻿namespace DocTime.Enums
{
    /// <summary>
    /// Статус должности
    /// </summary>
    public enum PositionStatus
    {
        /// <summary>
        /// Стажер
        /// </summary>
        Trainee,

        /// <summary>
        /// Штатный
        /// </summary>
        Staff,

        /// <summary>
        /// Зам. руководителя
        /// </summary>
        DeputyHead,

        /// <summary>
        /// Руководитель
        /// </summary>
        Supervisor
    }
}
