﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Domain.Enums
{
    public enum UserState
    {
        None = 0,
        Active = 1,
        /// <summary> В отпуске. </summary>
        Vacation = 2,
        /// <summary> Болен. </summary>
        Diseased = 3
    }
}
