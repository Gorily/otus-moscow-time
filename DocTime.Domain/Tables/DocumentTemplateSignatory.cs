﻿using DocTime.Domain.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace DocTime.Tables
{
    [Table("DocumentTemplateSignatories")]
    public class DocumentTemplateSignatory : BaseEntity
    {
        public int DocumentTemplateId { get; set; }
        public DocumentTemplate DocumentTemplate { get; set; }
        public int SignatoryTemplateId { get; set; }
        public SignatoryTemplate SignatoryTemplate { get; set; }
    }
}
