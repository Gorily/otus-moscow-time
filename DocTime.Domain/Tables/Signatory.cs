﻿using DocTime.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DocTime.Base.Interfaces;

namespace DocTime.Tables
{
    [Table("Signatories")]
    public class Signatory : BaseEntity, ISignatory, ISignatoryDocument
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int DocumentId { get; set; }
        public Document Document { get; set; }
        /// <summary> Подписан или нет. </summary>
        public bool Signed { get; set; }
        /// <summary> Дата подписи документа. </summary>
        public DateTime SignedDate { get; set; }
        /// <summary> Номер очередности подписи. </summary>
        public int SequenceNumber { get; set; }
    }
}
