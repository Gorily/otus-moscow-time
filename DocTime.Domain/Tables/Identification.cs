﻿using DocTime.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DocTime.Base.Interfaces;

namespace DocTime.Tables
{
    [Table("Identifications")]
    public class Identification : BaseEntity, IIdentification
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
