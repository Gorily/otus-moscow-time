﻿using DocTime.Domain.Base;
using DocTime.Domain.Base.Interfaces;
using DocTime.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocTime.Tables
{
    [Table("Users")]
    public class User : BaseEntity, IUser
    {
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(100)]
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime RegistrationDateUTC { get; set; }
        public Position Position { get; set; }
        public string Email { get; set; }
        public Department Department { get; set; }
        public virtual IEnumerable<UserDocument> Documents { get; set; }
        public UserState State { get; set; }
    }
}
