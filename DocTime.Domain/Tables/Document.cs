﻿using DocTime.Base.Interfaces;
using DocTime.Domain.Base;
using DocTime.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DocTime.Tables
{
    [Table("Documents")]
    public class Document : BaseEntity, IDocument
    {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public string File { get; set; }
        public DocumentState State { get; set; }
        public virtual IEnumerable<Signatory> Signatories { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, File: {File}, State: {State}, Signatories.Count: {Signatories.Count()}, DepartmentId: {DepartmentId}";
        }

        public string ToStringList(int listNumber)
        {
            return $"{listNumber}) id: {Id}, имя: {Name}.";
        }
    }
}
