﻿using DocTime.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocTime.Tables
{
    [Table("UserDocuments")]
    public class UserDocument : BaseEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int DocumentId { get; set; }
        public Document Document { get; set; }
    }
}
