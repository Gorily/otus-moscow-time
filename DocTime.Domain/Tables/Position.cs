﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DocTime.Base.Interfaces;
using DocTime.Domain.Base;

namespace DocTime.Tables
{
    [Table("Positions")]
    public class Position : BaseEntity, IPosition
    {
        public string Name { get; set; }
    }
}
