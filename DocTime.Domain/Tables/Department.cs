﻿using DocTime.Base.Interfaces;
using DocTime.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocTime.Tables
{
    [Table("Departments")]
    public class Department : BaseEntity, IDepartment
    {
        [Required]
        [StringLength(300)]
        public string Name { get; set; }
    }
}
