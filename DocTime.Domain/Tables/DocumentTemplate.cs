﻿using DocTime.Base.Interfaces;
using DocTime.Domain.Base;
using DocTime.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DocTime.Tables
{
    [Table("DocumentTemplates")]
    public class DocumentTemplate : BaseEntity, IDocumentTemplate
    {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public virtual Department Department { get; set; }
        public virtual IEnumerable<DocumentTemplateSignatory> Signatories { get; set; }
    }
}
