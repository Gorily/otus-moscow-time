﻿using DocTime.Base.Interfaces;
using DocTime.Domain.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Tables
{
    public class SignatoryTemplate : BaseEntity, ISignatory
    {
        public int DocumentId { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int SequenceNumber { get; set; }
    }
}
