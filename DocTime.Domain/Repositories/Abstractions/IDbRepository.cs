﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Repositories.Abstractions
{
    public interface IDbRepository
    {
        void SetNewPosition(User user, Position position);
        void AddDepartment(Department department);
        void AddDepartments(List<Department> departments);
        void AddDocument(Document document);
        void AddDocuments(List<Document> documents);
        void AddDocumentTemplate(DocumentTemplate documentTemplate);
        void AddSignatory(Signatory signatory);
        void AddSignatories(List<Signatory> signatories);
        void AddUserDocument(UserDocument userDocument);
        void AddUserDocuments(List<UserDocument> userDocuments);
        void AddPosition(Position position);
        void AddPositions(List<Position> positions);
        List<User> GetUsers();
        User GetUserById(int id);
        List<Document> GetDocuments();
        List<DocumentTemplate> GetDocTemplates();
        List<Signatory> GetSignatoriesByDocId(int id);
        Department GetDepartmentById(int id);
        void AddDocumentTemplates(List<DocumentTemplate> documentTemplates);
        void AddDocumentTemplatesSignatories(List<DocumentTemplateSignatory> fakeDocumentTemplatesSignatories);
    }

}
