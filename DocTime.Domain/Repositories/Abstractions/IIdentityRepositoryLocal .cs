﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Repositories.Abstractions
{
    public interface IIdentityRepositoryLocal
    {
        void AddUser(User user);
        void AddUsers(List<User> users);
        bool Access(string login, string password);
        void AddIdentification(Identification identification);
        void AddIdentifications(List<Identification> identifications);
    }
}
