﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocTime.Repositories.Abstractions
{
    public interface IIdentityRepository : IRepository<Identification>
    {
        Task<int?> AccessAsync(string login, string password);
    }
}
