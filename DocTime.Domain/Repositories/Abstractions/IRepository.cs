﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocTime.Repositories.Abstractions
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(int id);

        Task AddAsync(T entity);

        Task AddRangeAsync(IEnumerable<T> entities);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}
