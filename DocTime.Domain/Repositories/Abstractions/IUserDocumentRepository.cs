﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocTime.Repositories.Abstractions
{
    public interface IUserDocumentRepository : IRepository<UserDocument>
    {
        Task<UserDocument> GetUserDocumentByDocumentIdAsync(int documentId);
    }
}
