﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocTime.Repositories.Abstractions
{
    public interface IDocumentTemplateRepository : IRepository<DocumentTemplate>
    {
        Task<IEnumerable<DocumentTemplate>> GetTemplatesByDepartmentId(int departmentId);
    }
}
