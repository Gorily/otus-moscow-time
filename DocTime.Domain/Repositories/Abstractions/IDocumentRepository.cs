﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DocTime.DTO;

namespace DocTime.Repositories.Abstractions
{
    public interface IDocumentRepository : IRepository<Document>
    {
        Task<IEnumerable<Document>> GetDocsByDepartmentId(int departmentId);
        Task<IEnumerable<Document>> GetDocsRequiringSignatureByUser(UserDto user);
        Task<Document> GetByIdWithSignatoryUsersAsync(int id);
    }
}
