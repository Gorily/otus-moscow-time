﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Base.Interfaces
{
    public interface IPosition
    {
        string Name { get; set; }
    }
}
