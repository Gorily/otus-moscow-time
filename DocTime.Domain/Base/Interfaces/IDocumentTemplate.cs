﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Base.Interfaces
{
    public interface IDocumentTemplate
    {
        string Name { get; set; }
        Department Department { get; set; }
        IEnumerable<DocumentTemplateSignatory> Signatories { get; set; }
    }
}
