﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Base.Interfaces
{
    public interface ISignatoryDocument
    {
        /// <summary> Подписан или нет. </summary>
        bool Signed { get; set; }
        /// <summary> Дата подписи документа. </summary>
        DateTime SignedDate { get; set; }
    }
}
