﻿using DocTime.Enums;
using DocTime.Tables;
using System.Collections.Generic;

namespace DocTime.Base.Interfaces
{
    interface IDocument
    {
        public string Name { get; set; }
        string File { get; set; }
        DocumentState State { get; set; }
        IEnumerable<Signatory> Signatories { get; set; }
        Department Department { get; set; }
    }
}
