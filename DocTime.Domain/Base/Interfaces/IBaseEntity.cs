﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Domain.Base.Interfaces
{
    /// <summary> Сущность (база данных). </summary>
    public interface IBaseEntity
    {
        /// <summary> Идентификатор. </summary>
        int Id { get; set; }
    }
}
