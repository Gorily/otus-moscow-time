﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Base.Interfaces
{
    public interface IIdentification
    {
        int UserId { get; set; }
        string Login { get; set; }
        string Password { get; set; }
    }
}
