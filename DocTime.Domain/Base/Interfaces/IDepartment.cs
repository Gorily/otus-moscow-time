﻿namespace DocTime.Base.Interfaces
{
    interface IDepartment
    {
        string Name { get; set; }
    }
}
