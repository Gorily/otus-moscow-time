﻿using DocTime.Domain.Enums;
using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DocTime.Domain.Base.Interfaces
{
    public interface IUser
    {
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Patronymic { get; set; }
        DateTime BirthDate { get; set; }
        DateTime RegistrationDateUTC { get; set; }
        Position Position { get; set; }
        Department Department { get; set; }
        IEnumerable<UserDocument> Documents { get; set; }
        UserState State { get; set; }
    }
}
