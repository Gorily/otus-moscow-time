﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.Base.Interfaces
{
    public interface ISignatory
    {
        int DocumentId { get; set; }
        int UserId { get; set; }
        /// <summary> Номер очередности подписи. </summary>
        int SequenceNumber { get; set; }
    }
}
