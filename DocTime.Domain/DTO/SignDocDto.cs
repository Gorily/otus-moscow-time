﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.DTO
{
    public class SignDocDto
    {
        public SignDocDto(int userId, int documentId)
        {
            UserId = userId;
            DocumentId = documentId;
        }

        public int UserId { get; private set; }
        public int DocumentId { get; private set; }
    }
}
