﻿using System;
using System.Collections.Generic;
using System.Text;
using DocTime.Domain.Base;
using DocTime.Domain.Enums;
using DocTime.Tables;

namespace DocTime.DTO
{
    public class UserDto : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime RegistrationDateUTC { get; set; }
        public Position Position { get; set; }
        public string Email { get; set; }
        public Department Department { get; set; }
        public virtual IEnumerable<UserDocument> Documents { get; set; }
        public UserState State { get; set; }
    }
}
