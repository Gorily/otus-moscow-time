﻿using DocTime.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.DTO
{
    public class InitialDocDto
    {
        public InitialDocDto(User user, DocumentTemplate documentTemplate, string documentFile)
        {
            User = user;
            DocumentTemplate = documentTemplate;
            DocumentFile = documentFile;
        }
        public User User { get; set; }
        public DocumentTemplate DocumentTemplate { get; set; }
        public string DocumentFile{ get; set; }
    }
}
