﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.DTO
{
    public class LoginInfoDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
