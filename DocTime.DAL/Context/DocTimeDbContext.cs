﻿using DocTime.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocTime.DAL.Context
{
    public class DocTimeDbContext : DbContext
    {
        //static string ApplicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        //static string cs = $@"Data Source = (localdb)\MSSQLLocalDB; AttachDbFilename={ApplicationPath}\DocTime.mdf;Integrated Security = True";

        public DbSet<Department> Departments { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Signatory> Signatories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserDocument> UserDocuments { get; set; }
        public DbSet<Identification> Identifications { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<DocumentTemplate> DocumentTemplates { get; set; }
        public DbSet<DocumentTemplateSignatory> DocumentTemplateSignatories { get; set; }



        public DocTimeDbContext(DbContextOptions options) : base(options)
        { }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(cs);
        //}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDocument>()
                .HasOne(pt => pt.User)
                .WithMany(p => p.Documents)
                .HasForeignKey(pt => pt.UserId);

            modelBuilder.Entity<Signatory>()
                .HasOne(pt => pt.Document)
                .WithMany(p => p.Signatories)
                .HasForeignKey(pt => pt.DocumentId);

            modelBuilder.Entity<DocumentTemplateSignatory>()
                .HasOne(pt => pt.DocumentTemplate)
                .WithMany(p => p.Signatories)
                .HasForeignKey(pt => pt.DocumentTemplateId);
        }
    }
}
