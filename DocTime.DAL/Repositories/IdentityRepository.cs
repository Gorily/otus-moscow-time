﻿using DocTime.DAL.Context;
using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DocTime.DAL.Repositories
{
    public class IdentityRepository : IIdentityRepository
    {
        private readonly DocTimeDbContext _db;

        public IdentityRepository(DocTimeDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task<int?> AccessAsync(string login, string password)
        {
            var any = await _db.Identifications
                .SingleOrDefaultAsync(a => a.Login == login && a.Password == password);
            return any?.UserId;
        }

        public async Task AddAsync(Identification entity)
        {
            await _db.Identifications.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<Identification> entities)
        {
            await _db.Identifications.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Identification entity)
        {
            _db.Identifications.Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<Identification>> GetAllAsync()
        {
            return await _db.Identifications
                .ToArrayAsync();
        }

        public async Task<Identification> GetByIdAsync(int id)
        {
            return await _db.Identifications
            .SingleOrDefaultAsync(i => i.Id == id);
        }

        public async Task UpdateAsync(Identification entity)
        {
            _db.Identifications.Update(entity);
            await _db.SaveChangesAsync();
        }
    }
}
