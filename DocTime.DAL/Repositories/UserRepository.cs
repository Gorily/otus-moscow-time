﻿using DocTime.DAL.Context;
using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocTime.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly DocTimeDbContext _db;

        public UserRepository(DocTimeDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task AddAsync(User entity)
        {
            await _db.Users.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<User> entities)
        {
            await _db.Users.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(User entity)
        {
            _db.Users.Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _db.Users
                .Include(i => i.Position)
                .Include(i => i.Documents)
                .Include(i => i.Department)
                .ToArrayAsync();
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await _db.Users
            .Include(i => i.Department)
            .Include(i => i.Documents)
            .ThenInclude(t => t.Document)
            .Include(i => i.Position)
            .SingleOrDefaultAsync(i => i.Id == id);
        }

        public async Task UpdateAsync(User entity)
        {
            _db.Users.Update(entity);
            await _db.SaveChangesAsync();
        }
    }
}
