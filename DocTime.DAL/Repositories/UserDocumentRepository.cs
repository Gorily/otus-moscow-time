﻿using DocTime.DAL.Context;
using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocTime.DAL.Repositories
{
    public class UserDocumentRepository : IUserDocumentRepository
    {
        private readonly DocTimeDbContext _db;

        public UserDocumentRepository(DocTimeDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task AddAsync(UserDocument entity)
        {
            await _db.UserDocuments.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<UserDocument> entities)
        {
            await _db.UserDocuments.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(UserDocument entity)
        {
            _db.UserDocuments.Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task<UserDocument> GetUserDocumentByDocumentIdAsync(int documentId)
        {
            return await _db.UserDocuments
                    .Include(i => i.Document)
                    .Include(i => i.User)
                    .SingleOrDefaultAsync(f => f.DocumentId == documentId);
        }

        public async Task<IEnumerable<UserDocument>> GetAllAsync()
        {
            return await _db.UserDocuments.ToArrayAsync();
        }

        public async Task<UserDocument> GetByIdAsync(int id)
        {
            return await _db.UserDocuments
                .Include(i => i.Document)
                .Include(i => i.User)
                .SingleOrDefaultAsync(i => i.Id == id);
        }

        public async Task UpdateAsync(UserDocument entity)
        {
            _db.UserDocuments.Update(entity);
            await _db.SaveChangesAsync();
        }
    }
}
