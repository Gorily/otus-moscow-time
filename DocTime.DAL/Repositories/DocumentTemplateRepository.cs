﻿using DocTime.DAL.Context;
using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocTime.DAL.Repositories
{
    public class DocumentTemplateRepository : IDocumentTemplateRepository
    {
        private readonly DocTimeDbContext _db;

        public DocumentTemplateRepository(DocTimeDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task AddAsync(DocumentTemplate entity)
        {
            await _db.DocumentTemplates.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<DocumentTemplate> entities)
        {
            await _db.DocumentTemplates.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(DocumentTemplate entity)
        {
            _db.DocumentTemplates.Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<DocumentTemplate>> GetAllAsync()
        {
            return await _db.DocumentTemplates.ToArrayAsync();
        }

        public async Task<DocumentTemplate> GetByIdAsync(int id)
        {
            return await _db.DocumentTemplates.SingleOrDefaultAsync(i => i.Id == id);
        }

        public async Task<IEnumerable<DocumentTemplate>> GetTemplatesByDepartmentId(int departmentId)
        {
            return await _db.DocumentTemplates
                .Include(i => i.Department)
                .Include(i => i.Signatories)
                .ThenInclude(t => t.SignatoryTemplate)
                .ThenInclude(t => t.User)
                .Where(i => i.Department.Id == departmentId).ToArrayAsync();
        }

        public async Task UpdateAsync(DocumentTemplate entity)
        {
            _db.DocumentTemplates.Update(entity);
            await _db.SaveChangesAsync();
        }
    }
}
