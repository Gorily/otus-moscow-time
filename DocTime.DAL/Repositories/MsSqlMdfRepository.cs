﻿using DocTime.DAL.Context;
using DocTime.Domain.Enums;
using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocTime.DAL.Repositories
{
    public class MsSqlMdfRepository : IDbRepository, IDisposable
    {
        private readonly DocTimeDbContext _db;
        public MsSqlMdfRepository(DocTimeDbContext db)
        {
            _db = db;
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        public void SetNewPosition(User user, Position position)
        {
            throw new NotImplementedException();
        }
        public void AddSignatories(List<Signatory> signatories)
        {
            _db.Signatories.AddRange(signatories);
            _db.SaveChanges();
        }

        public void AddSignatory(Signatory signatory)
        {
            _db.Signatories.Add(signatory);
            _db.SaveChanges();
        }

        public void AddDepartment(Department department)
        {
            _db.Departments.Add(department);
            _db.SaveChanges();
        }

        public void AddDepartments(List<Department> departments)
        {
            _db.Departments.AddRange(departments);
            _db.SaveChanges();
        }

        public void AddDocument(Document document)
        {
            _db.Documents.Add(document);
            _db.SaveChanges();
        }

        public void AddDocuments(List<Document> documents)
        {
            _db.Documents.AddRange(documents);
            _db.SaveChanges();
        }

        public void AddUserDocument(UserDocument userDocument)
        {
            _db.UserDocuments.Add(userDocument);
            _db.SaveChanges();
        }

        public void AddUserDocuments(List<UserDocument> userDocuments)
        {
            _db.UserDocuments.AddRange(userDocuments);
            _db.SaveChanges();
        }

        public void AddPositions(List<Position> positions)
        {
            _db.Positions.AddRange(positions);
            _db.SaveChanges();
        }

        public void AddPosition(Position position)
        {
            _db.Positions.Add(position);
            _db.SaveChanges();
        }

        public List<User> GetUsers()
        {
            return _db.Users
                .Include(i => i.Department)
                .Include(i => i.Documents)
                .Include(i => i.Position)
                .ToList();
        }

        public List<Document> GetDocuments()
        {
            return _db.Documents
                .Include(i => i.Signatories)
                .ToList();
        }

        public List<Signatory> GetSignatoriesByDocId(int id)
        {
            return _db.Signatories
                .Where(w => w.DocumentId == id)
                .ToList();
        }

        public Department GetDepartmentById(int id)
        {
            return _db.Departments.FirstOrDefault(f => f.Id == id);
        }

        public void AddDocumentTemplate(DocumentTemplate documentTemplate)
        {
            _db.DocumentTemplates.Add(documentTemplate);
            _db.SaveChanges();
        }

        public List<DocumentTemplate> GetDocTemplates()
        {
            return _db.DocumentTemplates
                .Include(i => i.Signatories)
                .ToList();
        }

        public User GetUserById(int id)
        {
            return _db.Users
                .Include(i => i.Department)
                .Include(i => i.Documents)
                .ThenInclude(t => t.Document)
                .Include(i => i.Position)
                .SingleOrDefault(i => i.Id == id );
        }

        public void AddDocumentTemplates(List<DocumentTemplate> documentTemplates)
        {
            _db.DocumentTemplates.AddRange(documentTemplates);
            _db.SaveChanges();
        }

        public void AddDocumentTemplatesSignatories(List<DocumentTemplateSignatory> fakeDocumentTemplatesSignatories)
        {
            _db.DocumentTemplateSignatories.AddRange(fakeDocumentTemplatesSignatories);
            _db.SaveChanges();
        }
    }
}
