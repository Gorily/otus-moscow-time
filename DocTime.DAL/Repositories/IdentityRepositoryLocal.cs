﻿using DocTime.DAL.Context;
using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using System;
using System.Collections.Generic;

namespace DocTime.DAL.Repositories
{
    public class IdentityRepositoryLocal : IIdentityRepositoryLocal
    {
        private readonly DocTimeDbContext _db;
        public IdentityRepositoryLocal(DocTimeDbContext db)
        {
            _db = db;
        }
        public bool Access(string login, string password)
        {
            throw new NotImplementedException();
        }

        public void AddIdentification(Identification identification)
        {
            _db.Add(identification);
            _db.SaveChanges();
        }

        public void AddIdentifications(List<Identification> fakeIdenties)
        {
            _db.AddRange(fakeIdenties);
            _db.SaveChanges();
        }

        public void AddUser(User user)
        {
            _db.Add(user);
            _db.SaveChanges();
        }

        public void AddUsers(List<User> users)
        {
            _db.AddRange(users);
            _db.SaveChanges();
        }
    }
}
