﻿using DocTime.DAL.Context;
using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocTime.DTO;

namespace DocTime.DAL.Repositories
{
    public class DocumentRepository : IDocumentRepository
    {
        private readonly DocTimeDbContext _db;

        public DocumentRepository(DocTimeDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task AddAsync(Document entity)
        {
            await _db.Documents.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<Document> entities)
        {
            await _db.Documents.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Document entity)
        {
            _db.Documents.Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<Document>> GetAllAsync()
        {
            return await _db.Documents.ToArrayAsync();
        }

        public async Task<Document> GetByIdAsync(int id)
        {
            return await _db.Documents.SingleOrDefaultAsync(i => i.Id == id);
        }

        public async Task<Document> GetByIdWithSignatoryUsersAsync(int id)
        {
            return await _db.Documents
                .Include(i => i.Signatories)
                .ThenInclude(t => t.User)
                .SingleOrDefaultAsync(i => i.Id == id);
        }

        public async Task<IEnumerable<Document>> GetDocsByDepartmentId(int departmentId)
        {
            return await _db.Documents
                .Include(i => i.Department)
                .Include(i => i.Signatories)
                .Where(i => i.Department.Id == departmentId).ToArrayAsync();
        }

        public async Task<IEnumerable<Document>> GetDocsRequiringSignatureByUser(UserDto user)
        {
            var docs = _db.Documents
                .Include(i => i.Signatories)
                .ThenInclude(t => t.User).Where(w =>
                    w.State == Enums.DocumentState.Active
                    && w.Signatories.Any(a => a.UserId == user.Id && a.Signed == false));
            return await docs.ToArrayAsync();
        }

        public async Task UpdateAsync(Document entity)
        {
            _db.Documents.Update(entity);
            await _db.SaveChangesAsync();
        }
    }
}
