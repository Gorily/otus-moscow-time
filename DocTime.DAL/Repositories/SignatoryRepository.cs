﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DocTime.DAL.Context;
using DocTime.Repositories.Abstractions;
using DocTime.Tables;
using Microsoft.EntityFrameworkCore;

namespace DocTime.DAL.Repositories
{
    public class SignatoryRepository : IRepository<Signatory>
    {
        private readonly DocTimeDbContext _db;

        public SignatoryRepository(DocTimeDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task AddAsync(Signatory entity)
        {
            await _db.Signatories.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<Signatory> entities)
        {

            await _db.Signatories.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Signatory entity)
        {
            _db.Signatories.Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<Signatory>> GetAllAsync()
        {
            return await _db.Signatories
                .Include(i => i.Document)
                .Include(i => i.User)
                .ToArrayAsync();
        }

        public async Task<Signatory> GetByIdAsync(int id)
        {
            return await _db.Signatories
                .Include(i => i.Document)
                .Include(i => i.User)
                .SingleOrDefaultAsync(i => i.Id == id);
        }

        public async Task UpdateAsync(Signatory entity)
        {
            _db.Signatories.Update(entity);
            await _db.SaveChangesAsync();
        }
    }
}
