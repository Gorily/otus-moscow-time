﻿using DocTime.DAL.Context;
using DocTime.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace DocTime.WpfApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            IServiceCollection container = new ServiceCollection();

            var ApplicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var cs = $@"Data Source = (localdb)\MSSQLLocalDB; AttachDbFilename={ApplicationPath}\DocTime.mdf;Integrated Security = True";

            container.AddDbContext<DocTimeDbContext>(options => options.UseSqlServer(cs));
            container.AddScoped<MsSqlMdfRepository>();
            container.AddScoped<MainViewModel>();

            IServiceProvider serviceProvider = container.BuildServiceProvider();

            var mainWindow = new MainWindow()
            {
                DataContext = serviceProvider.GetService<MainViewModel>()
            };

            mainWindow.Show();
        }
    }
}
